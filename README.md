# A collection of thoughts about the challenges of moving to Backbone from an Angular background.

## The blog posts can be found under /docs

## The main file with the backbone code can be found at /public/index.html

Half working demo [here](http://answersonapostcard-47329.onmodulus.net "Half working demo")
