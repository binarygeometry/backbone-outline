I generally like to start with an interface and slowly bring data into the page as required. Backbone is a little intimidating, so this seems like a safe spot to jump off from.

Our basic functionality will need a control panel and a display panel. Coming from an Angular background, I imagine that I will be able to set dom element containers and use these to display route dependant partials. Before getting to there, we a method of updating the view as the user interacts with the page. 

```
  <div class="container">
    <div class="row">
      <nav class="col-md-6">
        <h1>Black Obilisk</h1>
        <ul>
          <li class="control-item"><a href="#home">Home</a></li> 
          <li class="control-item"><a href="#person/7">Person Seven</a></li> 
          <li class="control-item"><a href="#desk/sector6/desk5">Sector 6 Desk 5</a></li>  
        </ul>
      </nav>
      <article class="col-md-6">
        <h4>"In order to understand a problem you must first understand the solution that has been used to create it"<h4>
        <!-- rendered view goes here --><!-- yellow chrome aphorism blender kicks in! -->
        <section class="partial-wrapper" id="pw-main"></section>      </article>
    </div>
  </div>
```

lets try adding our two routes 

```
var workspace = workspace || {};

workspace.router = Backbone.Router.extend({

    routes: {
        'home': 'homeCtrl',              // #home
        'person/:id': 'personCtrl',      // #person/id
        'desk/:sector/:desk': 'deskCtrl'  // #pen/sector/desk
    },
 
    homeCtrl: function(){
        console.log('home');
    },
 
    personCtrl: function(id){
      console.log('person: ', id);
    },

    deskCtrl: function(sector, desk){
      console.log('sector: ', sector);
      console.log('desk', desk);
    }
}

(function ($) {
    router = new workspace.router();
    Backbone.history.start();
})(jQuery);
```

It's important to instantiate ([of a universal or abstract concept] be represented by an actual example.) an instance of the router class inside the document ready function. 

Backbone feels much nicer since I first abandoned it for Angular, I think the inclusion of descriptive nouns to be extended is a good feature.

The above example shows three different routing examples of increasing complexity. Backbone looks for the url paths and parameter variables inside a 'routes' object as a string based key, each 'url key' is provided with a controller which is described inside the same Router.extend configuration object.





