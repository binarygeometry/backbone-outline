Skipping over tests for a while since the real world is calling and we already spent half a day experimenting, lets write some more testable code to come back to.

At the moment we have an application that lists desks and sorts desks. It would be a useful to allow companies to add a desk to the database. Most people will prefer to do this via a form, so lets build one of those.

We already have a spare view available at the home route. That's to say it's currently displaying static data, and well, the default view is a great place to have people interact with your application.

What you have to remember when moving from Angular to Backbone, is that Backbone uses jQuery to handle many tasks. In Angular jQuery was available but considered really bad form. What this means for us today is that creating this form submission should be simple. Let's see!

Create the markup for the form
```
<script type="text/html" id="template-home">
  <div class='home'>
   <form id="add-desk">
    <input type="text" name="sector" placehoder="desk"/>
    <input type="text" name="desk" placeholder="desk"/>
    <input type="text" name="owner" placeholder="owner"/>
    <button class="btn btn-success" type="submit">Do Something!</a>
  </form>
  </div>
</script>
```
The Backbone.View class has an events property to which you can pass an object. The keys are the event to be listened for, and the value is a function to called when the event listener is triggered.


```
workSpace.homeView = Backbone.View.extend({
...
..
  events: {
    "submit": function (event) {
      event.preventDefault()
      console.log(event);
    }
  },

This works as expected. Next we want to move it out into a seperate function and build a dictionary from our data.

```
  events: {
    "submit": "createDesk"
  },
  
  createDesk: function (event) {
      event.preventDefault()

      var data = {
        owner : $('#add-desk input[name="owner"]').val(),
        desk  : $('#add-desk input[name="desk"]').val(),
        sector:$('#add-desk input[name="sector"]').val(),
      }
      console.log(data);
  },
```

Since we already created a Model for desks earlier, we can make use of that to persist our data to the server by passing our dictionary as the initialization object, and calling the inbuilt save method.

```
  createDesk: function (event) {
...
..
      var desk = new workSpace.Desk(data);
      desk.save();
  },
```

Backbone's save method usefully allows the promise.prototype.then() syntax. We can use this to update our view, or let the user know if there is a connectivity problem. It's quarter past late, so for now lets just assume we recieve a success message from the api

```
desk.save().then(function(stuff){
  console.log('good', stuff); //
  if (stuff.message !== "error"){
    // update view
    $('.home').html('<p class="success">Thanks,'+stuff.message)
  }
}); 
```

That works, but it's not very ux. Let's add some basic validation another day.





