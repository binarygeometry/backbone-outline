We need some tools

$npm install -save jsdom underscore backbone mocha chai sinon sinon-chai testem 

We need to configure Test 'em

$ nano testem.json
{
  "framework": "mocha",
  "src_files": [
    "public/js/components/jquery/dist/jquery.js",
    "public/js/components/underscore/underscore.js",
    "public/js/components/backbone/backbone.js",
    "public/js/app/main.js",
    "node_modules/chai/chai.js",
    "node_modules/sinon-chai/lib/sinon-chai.js",
    "node_modules/sinon/lib/sinon.js",
    "public/js/test/*.js"
  ]
}

And finally to move our code out of index.html and into js/app/main.js 
```
<script type="text/html" id="desk-view-main">
  <nav id="tweak"></nav>
  <div class='desk'>
    <h3>Workspace:</h3>
    <strong>Sector: <%= sector %>, owned by <%= owner %></strong>
  </div>
</script>
<!-- It should be loaded after all the template partials -->
<script src="js/app/main.js"></script>
```

$  node_modules/testem/testem.js 

Now you can view the test suite in your browser as well as the terminal http://localhost:7357/ 

This works, but you will probably notice an error related to underscore. This is most likely because we are including our underscore templates in the page. This is a horrible way to set out an application and one that is no longer useful for us.

$ npm install -save grunt-contrib-jst

$ mkdir js/app/templates/

$ nano !$test-template.tpl

Add the following to our grunt file

```
    grunt.initConfig({
...
..
      // precompile templates
      jst: {
        compile: {
          // options: {
            // templateSettings: {
              // interpolate : /\{\{(.+?)\}\}/g
            // }
          // },
          files: {
            // "path/to/compiled/templates.js": ["path/to/source/**/*.html"]
            "public/js/app/templates.js": ["public/js/app/templates/**/*.html"]
          }
        }
      },
...
..
    });

...
..
    grunt.loadNpmTasks('grunt-contrib-jst');

    // ===========================================================================
    // REGISTER TASKS ============================================================
    // ===========================================================================
    grunt.registerTask('default', ['jst', 'less', 'jshint', 'watch']);
}
```

$grunt --force # since the linter doesn't like the way I type and falls over everytime I save

We now have the reassuringly obscure content in our template.js file
```
this["JST"] = this["JST"] || {};

this["JST"]["public/js/app/templates/desk-view-main.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<!-- < -->\n<!-- script type="text/html" id="desk-view-main"> -->\n  <!-- <nav id="tweak"></nav> -->\n  <!-- <div class="desk"> -->\n    <h3>Workspace:</h3>\n    <!-- <strong>Sector: ' +
((__t = ( sector )) == null ? '' : __t) +
', owned by ' +
((__t = ( owner )) == null ? '' : __t) +
'</strong> -->\n  <!-- </div> -->\n<!-- </script> -->\n';

}
return __p
};
```

Well, we now have another addition to our stack. How do we begin working this back into our application?

I have managed to narrow down the error message I am receiving to one line.
    
    ReferenceError: property is not defined

    ((__t = ( property )) == null ? '' : __t) +

The View Controller is as follows (simplified for brevity)

    workSpace.viewMain = Backbone.View.extend({
    // WORKS
    // template: _.template($("#desk-view-main").html()), 
    // DOESNT WORK
    template: _.template(window.JST["public/js/app/templates/desk-view-main.html"]()), 
    // DOESNT WORK
    // template: _.template(window.JST["public/js/app/templates/desk-view-main.html"](), {"owner": owner, "sector": sector}), 
    el: '#pw-main',

    initialize: function() {
      this.render();
    },

    render: function(a) {
      var dictionary = {"property", "value"}
      var html = that.template(dictionary);
      $(that.el).append(html);
    }

    });

The compiled file looks as follows

    this["JST"] = this["JST"] || {};

    this["JST"]["public/js/app/templates/desk-view-main.html"] = function(obj) {
    obj || (obj = {});
    var __t, __p = '', __e = _.escape;
    with (obj) {
    __p += '  <nav id="tweak"></nav>\n  <div class="desk">\n    <h3>Workspace:</h3>\n    <strong> ' +
    ((__t = ( property )) == null ? '' : __t) +
    '</strong>\n\n    <h3>end Workspace:</h3>\n  </div>\n</div>\n';

    }
    return __p
    };


