So, our thing to do now is, well, sleep. Sleep often doesn't really seem like an option when you're a student so REST will have to do.

Looking at the model we created in part 4, we can see that we need the following structure

    sector: 0,
    desk: 0,
    owner: null,
    occupied: false,
    occupied_by: null

Our express app uses mongoose. Lets create a file models/Desk.js
```
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var DeskSchema = new Schema({
    sector      : String,
    desk        : String,
    owner       : String,
    occupied    : String,
    occupied_by : String
});

module.exports = mongoose.model('Desk', DeskSchema);
```

```
var Desk= require('../models/Desk');

// res.json({ message: 'hooray! welcome to our api!' }); 
exports.addDesk = function(req, res) {
  // var owner   = req.params.user_id
  var desk    = req.body.desk    || false
  ,   sector   = req.body.sector   || false
  ,   owner    = req.body.owner    || false
  ,   occupied   = req.body.occupied   || false
  ,   occupied_by  = req.body.occupied_by  || false

  if (!desk || !sector || !owner || !occupied || !occupied_by) {
    res.json({ 
      message : 'You missed a bit!',
      desk    : desk,
      sector   : sector,
      owner    : owner,
      occupied   : occupied,
      occupied_by  : occupied_by
    })
  }
  else {
    var desk = new Desk()      // create a new instance of the Bear model
    desk.owner   = owner
    desk.desk    = desk   // set the desks name (comes from the request)
    desk.sector   = sector
    desk.occupied    = occupied
    desk.occupied_by = occupied_by
    // save the desk and check for errors
    bear.save(function(err) {
        if (err)
            res.send(err);

        res.json({ message: 'Bear created! with id '+ desk.owner });
    });
  }
}

exports.getDesks = function(req, res) {
    console.log('getDesks')
    // res.json({ message: 'get products!' }); 
    Desk.find(function(err, desks) {
        if (err)
            res.send(err);

        res.json(desks);
    });
}

exports.getDeskById = function(req, res) {
    console.log('getDesksById')
    // res.json({ message: req.params }); 
        Bear.findById(req.params.product_id, function(err, bear) {
            if (err)
                res.send(err);
            res.json(bear);
        });
}
exports.updateDeskById = function(req, res) {

  Desk.findById(req.params._id, function(err, desk) {

    if (err)
        res.send(err);

        desk.owner   = owner
        desk.desk    = desk   // set the desks name (comes from the request)
        desk.sector   = sector
        desk.occupied    = occupied
        desk.occupied_by = occupied_bybear
        desk.save(function(err) {
          if (err)
            res.send(err);

          res.json({ message: 'Successfully updated' });
        });
  });
}
```
And finally in our server definition file app.js
```
var deskController = require('./controllers/desk');
// get all products
app.get('/api/desk', deskController.getDesks)
// get one products
app.get('/api/desk/:desk_id', deskController.getDeskById)
// creat product
app.post('/api/desk/:desk_id', deskController.addDesk)
// app.post('/quick/:product', manageController.addProduct)
// update product by id
app.put('/api/desk/:desk_id', deskController.updateDeskById)
// delate product by id
// app.delete('/api/desk/:desk_id', deskController.deleteDeskById)
```