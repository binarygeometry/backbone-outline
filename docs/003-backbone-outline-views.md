Now we have a simple menu of hardcoded links, when we click them we can view the url parameters as a console log. It would be nicer if we could actually see this same information rendered in the html. Eventually we are going to use a Collection of items built of data returned from our api via a Model. For now it will simply do to introduce a templating system that will act as a place to put our data when we get to that stage.

First off we create an html element inside of a script tag, it will be available for use inside our javascript but invisible on the rendered DOM. It's a quick way to get some working functionality onto the page, perhaps not as neat or extensible as a template engine and seperate partial files, but perfect for our usage just now. Our app has three main states that will be controlled by the router, home, person, desk. We don't know what the person will do at the desk when they are not at home, just that a typical app will have an default state (perhaps listing notifications or a CTA) and usually a settings/profile section as well as a secondary state where the actual functionality of the product will be controlled. Note that these must be included in the page before we include our application logic otherwise underscore will throw an unrecognised type error.

```
<script type="text/html" id="template-home">
  <div class='home'>
    <h2>Welcome Back @~USER~@</h2>
    <a class="btn" href="#">Do Something!</a>
  </div>
</script>
<script type="text/html" id="template-desk">
  <div class='person'>
    <h2>Hi User Indentication No:<%= user %></h2>
    <p>Your Account Is currently @~act_status~@</p>
  </div>
</script>
<script type="text/html" id="template-desk">
  <div class='desk'>
    <h3>You are currently allocated workspace</h3>
    <strong>Sector <%= sector %></strong>
    <span>Desk <%= desk %></span>
  </div>
</script>
```

Now we have some vague approximation of a partial system implemented we can create our three top level views for the different parts of our application. You will notice that since we have not introduced Models into our system yet, we have simply hardcoded the dictionary objects passed to our templates for interpolation.

```
workspace.homeView = Backbone.View.extend({

  // This uses underscores _.template function to create a DOM element from the markup hidden inside <script id="template-home">
  template: _.template($("#template-home").html()),

  // This is the DOM element to which the view will be attached
  el: '#pw-main',

  // This is called as part of the Backbone teams implementation of POJO Class spoofing. 
  // It is called automatically when a 'new' instance of the workspace.homeView object is created. 
  initialize: function() {
    this.render();
  },

  // This is the method called by the init function, it uses jQuery to update the view element with the correct template
  render: function() {
    var html = this.template();
    // Append the result to the view's element.
    $(this.el).html(html);

  }
});
workspace.personView = Backbone.View.extend({
  template: _.template($("#template-person").html()),

  el: '#pw-main',

    // tagName: "li",
  initialize: function() {
    // _.bindAll(this, "alertName");
    this.render();
  },

  render: function() {
    // This is a dictionary object of the attributes of the models.
    // => 
    // var dict = this.model.toJSON(); 
    var dict = { user: "7", status: "active" }

    // Pass this object onto the template function.
    // This returns an HTML string.
    var html = this.template(dict);

    // Append the result to the view's element.
    $(this.el).html(html);

  }
});

workspace.deskView = Backbone.View.extend({
  template: _.template($("#template-desk").html()),

  el: '#pw-main',

    // tagName: "li",
  initialize: function() {
    // _.bindAll(this, "alertName");
    this.render();
  },

  render: function() {
    // This is a dictionary object of the attributes of the models.
    // => 
    // var dict = this.model.toJSON(); 
    var dict = { sector: "7", desk: "4" }

    // Pass this object onto the template function.
    // This returns an HTML string.
    var html = this.template(dict);

    // Append the result to the view's element.
    $(this.el).html(html);

  }
});
```


``
Finally we can add assign reference to our view objects inside the corresponding route controllers we created in the proceeding section.

```
    homeCtrl: function(){
      var deskView = new workspace.homeView();
    },
 
    personCtrl: function(id){
      this.view = new workspace.personView();
    },

    deskCtrl: function(sector, desk){
      this.view = new workspace.deskView();
    }
```



NOTE: Later we will have to revisit this code to make it zombie proof, but not today
https://lostechies.com/derickbailey/2011/09/15/zombies-run-managing-page-transitions-in-backbone-apps/