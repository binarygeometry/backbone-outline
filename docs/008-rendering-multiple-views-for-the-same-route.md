We now have data on the page! Hopefully you will agree that's quite a nice feeling. Now we have data lets look at some ways to filter it. Once we have done that we can spend a little time reorganising our app, and perhaps splitting out our code so we are no longer working with a monolithic index.html file.

Most the tutorials I have read seem to indicate that Backbone works on a more 'just javascript' paradigm, so I'm first going to explore this hypothesis by splitting our main desk route into two seperate views, one which will display the available filters, the other, the results of the filtering.

Turns out it is as simple as calling multiple views from the router controller
```
    deskCtrl: function(sector, desk){
      var that = this
      var desk = new workSpace.DeskCollection();

      desk.fetch().then( function(resp) {// fetch client collection from db
        // console.log('deskCtrl', resp)
        that.view = new workSpace.deskViewNav({ collection: desk });
        that.view = new workSpace.deskViewMain({ collection: desk });
      });
    }
```

DeskViewMain, be essentially be our view/controller from the last section
```
workSpace.deskViewMain = Backbone.View.extend({
  template: _.template($("#desk-view-main").html()),

  el: '#pw-main',

  initialize: function() {
    this.render();
  },

  render: function() {
    var that = this
    // mow down zommbies    
    $(this.el).empty();
    // iterate over the collection object
    this.collection.each(function(desk){
      console.log(desk)
      var owner = desk.get('owner');
      var sector = desk.get('sector');
      var dict = {"owner": owner, "sector": sector}
      console.log(dict)
      var html = that.template(dict);
      console.log(html)
      $(that.el).append(html);
    })
  }
});
```
```
<script type="text/html" id="desk-view-main">
  <div class='desk'>
    <h3>Workspace:</h3>
    <strong>Sector: <%= sector %>, owned by <%= owner %></strong>
  </div>
</script>
```

DeskViewNav is a little more involved. Because we want to render a select select element with dymanically generated options, we need a way to run our each function inside the template. This takes a little tweaking to work out but isn't overly complicated once you work out the syntax

```
workSpace.deskViewNav = Backbone.View.extend({
  template: _.template($("#desk-view-nav").html()),

  el: '#pw-nav',

  initialize: function() {
    this.render();
  },

  render: function() {
    var that = this
    // mow down zommbies    
    $(this.el).empty();
    // store the collection object
    var desk =  this.collection
    var html = that.template({desks: desk.toJSON()});
    // this.collection.each(function(desk){
      // console.log(desk)
    // })
      $(this.el).html(html);
  }
});
```
```
<script type="text/html" id="desk-view-nav">
  <div class='desk'>
    <h3>Filter Workspace by Owner:</h3>
    <% _.each(desks, function(desk){ %> 
    <%= desk.owner %>
  <% }); %>
  </div>
</script>
```

We have our basic building blocks in place. Lets put them together in the next section and construct our first 'feature'. Before we do so you may have noticed that our pw-nav section is not being removed when we switch to one of our other routes. A simple hack to get round this problem would be to change the view element of the deskViewNav.
```
workSpace.deskViewNav = Backbone.View.extend({
  template: _.template($("#desk-view-nav").html()),

  el: '#pw-main #tweak',
```
And then to add this inside the '#desk-view-main' template, in this way approximating Angulars ui-sref. 
```
<script type="text/html" id="desk-view-main">
  <nav id="tweak"></nav>
  <div class='desk'>
    <h3>Workspace:</h3>
    <strong>Sector: <%= sector %>, owned by <%= owner %></strong>
  </div>
</script>
```

While it doesn't throw any errors it's important to put the element in the correct order inside the route controller, as the second one is a dependant of the first. I have no idea if this is a potential bug as our app gets more complicated and perhaps our different views are calling different asychnoious data. Just now though it's a feature.

```
    deskCtrl: function(sector, desk){
      var that = this
      var desk = new workSpace.DeskCollection();

      desk.fetch().then( function(resp) {// fetch client collection from db
        // console.log('deskCtrl', resp)
        that.view = new workSpace.deskViewMain({ collection: desk });
        that.view = new workSpace.deskViewNav({ collection: desk });
      });
    }
```