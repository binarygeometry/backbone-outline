Currently we have a very basic list of Companies who provide desk space. We need to turn this into an interface element, so the first thing we need to do is to remove the duplicates from the list and then we can assign each company as an option on an element.

That was fairly straightforwardly acomplished by applying underscores .uniq method to the collection before passing it to the View

```
//workSpace.deskViewNav = Backbone.View.extend({ ...
  render:{ ...
    var desk =  this.collection.toJSON()
    var clean_desk = _.uniq(desk, 'owner');
    var html = that.template({desks: clean_desk});
```
```
<script type="text/html" id="desk-view-nav">
  <div class='desk'>
    <h3>Filter Workspace by Owner:</h3>
    <select name="filter_owner">
    <option disabled selected> -- select an option -- </option>
    <% _.each(desks, function(desk){ %> 
      <option value="<%= desk.owner %>"><%= desk.owner %></option>
    <% }); %>
    </select>
  </div>
</script>
```

We now need a way to intercept the the change event on this select element. Since our target audience is the mobile phone market (the app will run inside iconic) we happily don't have to worry about IE8 not supporting change events. Yippee

```
//workSpace.deskViewNav = Backbone.View.extend({ ...
  events: {
        "change select": "activateFilter"
  },
  
  activateFilter: function() {
    var filter = $('select').val();
    console.log(filter)
        // do something on select change
  }
```

So far so good! Let's search the internet for a sensible looking pubSub pattern. 
https://blog.safaribooksonline.com/2013/10/02/decoupling-backbone-applications-with-pubsub/

This little snippet looks really helpful
```
var pubSub = _.extend({},Backbone.Events);
```

We can wire into our views as so

```
workSpace.deskViewMain = Backbone.View.extend({
  ...
  initialize: function() {
    this.render();
    this.listenTo(pubSub,"foo:thing",this.bla,this);

  },
  bla: function(jfkld){
    console.log(jfkld.filter) // "Space SubLets"
  },
```
```
workSpace.deskViewNav = Backbone.View.extend({
  ...
  events: {
        "change select": "activateFilter"
  },
  
  activateFilter: function() {
    var filter = $('select').val();
    console.log(filter)
    pubSub.trigger("foo:thing",{
      filter : filter
    });
        // do something on select change
  }
```

All we need to do now is find a way to repaint our deskViewMain using the new parameter 

```
  bla: function(jfkld){
    console.log('call', jfkld);
    var filter = jfkld.filter;
    this.render(filter)
  },

  render: function(a) {
    a = typeof a !== 'undefined' ? a : 42;
    var that = this
    // mow down zommbies    
    $(this.el).empty();
    // iterate over the collection object
    this.collection.each(function(desk){
        
      console.log(desk)
      var owner = desk.get('owner');
      var sector = desk.get('sector');
      var dict = {"owner": owner, "sector": sector}
      if(a === 42){
        var html = that.template(dict);
        $(that.el).append(html);
        
      } else {
        if (dict.owner === a) {
          var html = that.template(dict);
          $(that.el).append(html);

        }
      }
      console.log(dict)
      console.log(html)
    })
  }
```

Works quite well, although we will now have to rewrite our view slightly to avoid throwing the navigation out with the zombie disposal.

Before we try that tho it would not be prudent to go any further without writing some tests.

