It's starting to feel a little an application now. I'm quite enjoying this back to front way of exploring a frontend framework, it's been a nice gentle introduction to get to grips with the way Backbone handles things. It is a sold as flexible unopinionated framework and so far it seems to live up to this claim. Still, if all we wanted was to move some static DOM elements around we wouldn't be here, so now seems an apt time to take a look at understand Backbones conception of a Model.

At it's simplest a Model is essentially an approximation of a Class, perhaps javascript has now evolved to the point where we can actually just call it a class for brevity without invoking too much hatred from those in the non implicetly typed world.

It has a few built in function for assigning and working with attributes stored in the Model. 
has('dimension') // false
set({'dimension':'1.5x3x3'})
get('dimension') // 1.5x3x3

It can be assigned methods, an example of working with a simple implementation can be seen below.

workSpace.Desk = Backbone.Model.extend({
  defaults: {
    sector: 0,
    desk: 0,
    owner: null,
    occupied: false,
    occupied_by: null
  },
  initialize: function(){
    alert("Welcome to Space SubLets");
  },
  book: function( occupier ){
    this.set({ occupied: true, occupied_by: occupier });
  },
  free: function () {
    this.set({ occupied: false, occupied_by: null });
  },
  displayStatus: function(){
    if (this.get('occupied')){
      console.log( 'Desk: ' + this.get('sector') + '.' + this.get('desk') + ' is occupied by: ' + this.get('occupied_by') );
    }
    else {
      console.log( 'Desk available, please contact ' + this.get('owner') );
    }
  }
});

var desk = new workSpace.Desk({ sector: 78, desk: 67, owner: 'Space SubLets'});
desk.displayStatus(); // Desk available, please contact Space SubLets
desk.book('674563'); 
desk.displayStatus(); // Desk: 78.67 is occupied by: 674563
desk.free(); 
desk.displayStatus(); // Desk available, please contact Space SubLets

So far so useless. What we are really concerned about is state manipulation abilities that Backbone gives us, the bindings between the view and our data. Thankfully Backbone employs a useful pub-sub pattern.

```
  initialize: function(){
    alert("Welcome to Space SubLets");
    this.on("change:occupied", function(model){
        model.displayStatus()
    });
  },

Now when the status of one of our desks changes, we are notified of the details. Well, not really. At the moment all we are doing is logging this to the console. In our final product our displayStatus method will have to actually do something. If we wanted to be really advanced (and a little rutheless), we could use the internet of things to automatically free a desk if it's occupier takes their phone away from the geolocation of the desk for over ten minutes unscheduled desk, and notify another deskless user in our dystopian hotdesking environment waiting that one is now available. For today however we are simply going to set up a simple REST api using Node and Express which we can then use to explore the built in CRUD facilities of Backbone Models.


















http://codepen.io/KryptoniteDove/blog/load-json-file-locally-using-pure-javascript