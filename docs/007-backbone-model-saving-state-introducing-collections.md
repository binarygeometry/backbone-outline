So, we have a hopefully functioning api. Lets hook it into our Backbone Model.

```
workSpace.Desk = Backbone.Model.extend({
  defaults: {
    sector: 0,
    desk: 0,
    owner: null,
    occupied: false,
    occupied_by: null
  },
 
  url : function() {
    // Important! It's got to know where to send its REST calls. 
    // In this case, POST to '/donuts' and PUT to '/donuts/:id'
    return this.id ? '/api/desk/' + this.id : '/api/desk'; 
  } 
```

We don't have any data yet, so naturally we want to create some. 

var desk = new workSpace.Desk({ sector: 78, desk: 67, owner: 'Space SubLets'});
desk.save();

Surprisingly, it works.

Now we have some data and a server to work with we can have a look at Backbone Collections.

workSpace.Desks= Backbone.Collection.extend({
  model : workSpace.Desk,
  url : "/api/desk"
});
 
var desks = new workSpace.Desks;
 
desks.fetch(); // this makes a call to the server and populates the collection based on the response.


This returns a json object of our collection. This point is where, coming from an angular background, things start to get really quite complicated, not difficult exactly, just more verbous and tedious. I think there will be a payoff soon, whvere the actual flexiblity of Backbones unopinionated architecture means there are less hoops to jump through to make it work the way you need, but for now, it's defiantely a peak in the learning curve.

One of the gotchas I encountered was remembering to pass the collection object (rather than the data returned from the asychronious request) to the view

    deskCtrl: function(sector, desk){
      var that = this
      var desk = new workSpace.DeskCollection();
      desk.fetch().then( function(resp) {// fetch client collection from db
        console.log('deskCtrl', resp)
        //<!-- that.view = new workSpace.deskView({ collection: resp }); // doesnt work
        that.view = new workSpace.deskView({ collection: desk });
      });
    }

Now we create our render template as follows

```
  render: function() {
    var that = this
    // mow down zommbies    
    $(this.el).empty();
    // iterate over the collection object
    this.collection.each(function(desk){
      console.log(desk)
      var owner = desk.get('owner');
      var sector = desk.get('sector');
      var dict = {"owner": owner, "sector": sector}
      console.log(dict)
      var html = that.template(dict);
      console.log(html)
      $(that.el).append(html);
      // var dict = { sector: "7", desk: "4" }
    })
```