Loading files from a cdn is nice, it's a little choppy for local development however and if it's threatening sunshine outside having the files installed locally means you can nip out the park for an hour or two without interupting your workflow.

Assuming you have a development environment installed or you are capable of installing one lets just dive right in.

$ bower init # runs you through a file creation script - choosing mostly defaults - creates 'bower.json'
$ echo '{ "directory" : "public/components" }' > .bowerrc  # creates .bowerrc and places installed packages in custom folder
$ bower install -save jquery underscore backbone # installs these into the public/components location we specify

$ echo "public/components/" > .gitignore # create .gitingore - ignore our locally installed dependancies
$ git init && git add -A && git status # should display .bowerrc bower.json .gitignore index.html 
$ git commit -m "set up front end dependancies"

$ nano package.json # opens a newly created file for editing - ctrl+shift+v to paste - ctrl-x to save and exit
```
  {
    "name": "getting-started",
    "version": "0.1.0",
    "devDependencies": {
      "grunt": "~0.4.4",
      "grunt-contrib-jshint": "latest",
      "jshint-stylish": "latest",
      "grunt-contrib-less": "latest",
      "grunt-contrib-watch": "latest"
    }
  }
```

$ npm install # downloads devDependancies into 'node_modules' folder
$ echo "node_modules" >> .gitignore # echo's newline to end of .gitignore 
$ nano gruntfile.js # https://scotch.io/tutorials/a-simple-guide-to-getting-started-with-grunt
```
  // Gruntfile.js

  // our wrapper function (required by grunt and its plugins)
  // all configuration goes inside this function
  module.exports = function(grunt) {

    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({

      // get the configuration info from package.json ----------------------------
      // this way we can use things like name and version (pkg.name)
      pkg: grunt.file.readJSON('package.json'),

      // all of our configuration will go here

      // configure jshint to validate js files -----------------------------------
      jshint: {
        options: {
          reporter: require('jshint-stylish') // use jshint-stylish to make our errors look and read good
        },

        // when this task is run, lint the Gruntfile and all js files in src
        build: ['Gruntfile.js', 'public/js/**/*.js']
      },
      less: {
        development: {
          options: {
            compress: true,
            yuicompress: true,
            optimization: 2
          },
          files: {
            "public/css/main.css": "public/less/main.less" // destination file and source file
          }
        }
      },
      watch: {
        styles: {
          files: ['public/less/**/*.less', 'public/js/**/*.js'], // which files to watch
          tasks: ['less', 'jshint'],
          options: {
            nospawn: true
          }
        }
      }

    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    // we can only load these if they are in our package.json
    // make sure you have run npm install so our app can find these
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // ===========================================================================
    // REGISTER TASKS ============================================================
    // ===========================================================================
    grunt.registerTask('default', ['less', 'jshint', 'watch']);
}
```

Quite alot going here but essentially we now have a very basic build process which will compile our LESS into CSS and check our javascript for syntax errors. The 'watch' task lets us specify which locations we are keeping track of

$ grunt --force # This extra flag is necessary since by default jshint will cause the task to exit on error

$ git add . && git commit -m "Added folder structure and grunt based build process for less and to check for errors in app js file"


When kicking of a new project, it's useful to include the files manually in html page, it's not worth adding them to our build process at this stage, since we don't really know what we are doing and how we are going to stucture our app. 

```
<script src="public/components/jquery/dist/jquery.js"></script>
<script src="public/components/underscore/underscore.js"></script>
<script src="public/components/backbone/backbone.js"></script>
```

What totally is worth doing is opening up each of those files and checking which version you are using. You can then update your package.json file with the correct version and this hopefully avoid you having any trouble when deploying your app on a different system or at a different point. It's easy to imagine a dark future where developers are forced to hoard old versions of software after a rogue unrecoverable error takes down the whole npm system. For now though it's simply a case of noting the version numbers and adding them to bower.json or at least confirming to yourself that they are there.

```
"dependencies": {
  "jquery": "~2.1.4",
  "underscore": "~1.8.3",
  "backbone": "~1.2.1"
}
```

