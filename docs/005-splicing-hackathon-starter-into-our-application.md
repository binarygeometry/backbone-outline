For this demo, we are going to make use of the great Hackathon-Starter available on github. If we don't fuck up (if the OED can change the definition of Terrorism from 'Government by intimidation' to the one that's currently floating about, then well, I think the occasionaly 'fuck up' is worksafe) majorly, hopefully our Grunt based build system will still work when we have finished splicing the two apps together. 

getting-started$ mv package.json getting-started.package.json  

getting-started$ cp ../hackathon-starter/package.json .

Now we can manually copy our grunt deps onto the end of the new file
```
{
  "name": "hackathon-starter",
  "version": "3.0.2",
  "repository": {
    "type": "git",
    "url": "https://github.com/sahat/hackathon-starter.git"
  },
  
  .......

  "devDependencies": {
    "blessed": "^0.1.56",
    "chai": "^2.3.0",
    "mocha": "^2.2.5",
    "multiline": "^1.0.2",
    "supertest": "^1.0.1",
  }
}
```
```
{
  "name": "getting-started",
  "version": "0.1.0",
  "repository": {
    "type": "git"
  },
  
  .......

  "devDependencies": {
    "blessed": "^0.1.56",
    "chai": "^2.3.0",
    "mocha": "^2.2.5",
    "multiline": "^1.0.2",
    "supertest": "^1.0.1",
    "grunt": "~0.4.4",
    "grunt-contrib-jshint": "latest",
    "jshint-stylish": "latest",
    "grunt-contrib-less": "latest",
    "grunt-contrib-watch": "latest"
  }
}
```

getting-started$ npm install

Will spiral around frustratingly, 

getting-started$ grunt

Will remind us that we don't always use semi-colons for brevity, but that we should really add them in to keep our boss happy. In reality all of our code is still contained within a script tag in the index.html file, and thankfully our application hasn't broken yet.

getting-started$ mv public/ getting-started-public/ # make a save of our files so they don't get overwritten

getting-started$ cp -r ../hackathon-starter/public ../hackathon-starter/app.js ../hackathon-starter/README.md ../hackathon-starter/config ../hackathon-starter/controllers ../hackathon-starter/models ../hackathon-starter/tests ../hackathon-starter/uploads ../hackathon-starter/views ../hackathon-starter/uploads ../hackathon-starter/ ./ # copy all the required files from the hackathon starter

getting-started$ node app.js

You should now be looking at the default set up for the hackathon starter. If you didn't install robomongo yet, or set up a modulus.io account to store your database because setting up a local mongo instance is too much like procastination, then do so now, and come back when you have no errors, and you can view the default hackathon starter kit at localhost:3000

$ echo 'config/secrets.js' >> .gitignore  ## ignore our db config file

Now open app.js from the starter kit. It contains loads of goodness like user authentication and jade templating plugins. That's over complicating things at the moment. What we really want is simply to serve the index.html file we have been working with all along, but doing so in a way that we can communicate with our app via ajax without any cross domain complications.

Comment out this bit

```
// app.use(lusca({
//   csrf: true,
//   xframe: 'SAMEORIGIN',
//   xssProtection: true
// }));
```

Change this bit
```
// app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));
```

Now following 

```
/**
 * Primary app routes.
 */
app.get('/', homeController.index);
```

We can open controllers/home.js and make the following changes
```
/**
 * GET /
 * Home page.
 */
exports.index = function(req, res) {
  res.sendfile('./public/index.html');
  // res.render('home', {
    // title: 'Home'
  // });
};
```

Finallyish, 

getting-started$ mv index.html public/

getting-started$ node app.js

And breath a sigh of relief, it works!

There is a little more to do however. You will remember we saved a copy of our original public directory to getting-started-public. Let's merge that in now

getting-started$ cp -r getting-started-public/* public/ 

And move the components into the publicly accessible js folder

getting-started$ mv public/components public/js/

Finally we need to update our .bowerrc
```
{ "directory" : "public/js/components" }
```

And change the path in our index.html
```
<script src="js/components/jquery/dist/jquery.js"></script>
<script src="js/components/underscore/underscore.js"></script>
<script src="js/components/backbone/backbone.js"></script>
```

Notice that we have removed public from the url path, since all paths are relative to the statically served asset.

Last thing to keep the working tree clean

$ echo "public/js/bower_components/" >> .gitignore
$ echo "public/js/components/" >> .gitignore














