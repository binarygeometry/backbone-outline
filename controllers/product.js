var Product = require('../models/Product');

/**
 * GET /login
 * Login page.
 */
exports.getProductBy = function(req, res) {
  
  Product.findById(req.params.product_id, function(err, bear) {
      if (err)
          res.send(err);
      res.json(bear);
  });
};