var User = require('../models/User');
var Bear = require('../models/Product');

/**
 * GET /shop
 * List all shopkeepers
 */
exports.getUserProfiles = function(req, res) {
  // res.json({ message: 'hooray! welcome to our api!' });   
    // res.json({ message: 'get products!' }); 
    User.find({}, 'profile', function(err, users) {
        if (err)
            res.send(err);

        res.json(users);
    });
};

/**
 * GET /shop/:id
 * List all shopkeepers
 */
exports.getShopById = function(req, res) {
  // res.json({ message: 'hooray! welcome to our api!' });   
    // res.json({ message: 'get products!' }); 
    var shopData = {
        "owner" : {},
        "products" : []
    }

    User.findById(req.params.owner_id, function(err, shop) {
        if (err)
            res.send(err);

        shopData.owner = shop.profile
        
        Bear.find({owner: req.params.owner_id}, function(err, products) {
            if (err)
                res.send(err)
            shopData.products = products
            // res.json(products)
            shopData.thing = 'tewe'
            res.json(shopData);
        })
    });
};

exports.displayShop = function(req, res){

  res.render('shop', {
    title: 'shop: '+req.params.owner_id,
    owner: req.params.owner_id
  });
}

exports.how = function(req, res){

  res.render('about', {
    title: 'How it works'//: '+req.params.owner_id,
    // owner: req.params.owner_id
  });
}


// blockchain payment gateway
// var http = require('http');

// exports.sendToBlockChain = function(req, res){

//     http.get({
//         host: 'personatestuser.org',
//         path: '/email'
//     }, function(response) {
//         // Continuously update stream with data
//         var body = '';
//         response.on('data', function(d) {
//             body += d;
//         });
//         response.on('end', function() {

//             // Data reception is done, do whatever with it!
//             var parsed = JSON.parse(body);
//             callback({
//                 email: parsed.email,
//                 password: parsed.pass
//             });
//         });
//     });
// }