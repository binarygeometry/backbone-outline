  // Gruntfile.js

  // our wrapper function (required by grunt and its plugins)
  // all configuration goes inside this function
  module.exports = function(grunt) {

    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({

      // get the configuration info from package.json ----------------------------
      // this way we can use things like name and version (pkg.name)
      pkg: grunt.file.readJSON('package.json'),

      // all of our configuration will go here

      // precompile templates
      jst: {
        compile: {
          // options: {
            // templateSettings: {
              // interpolate : /\{\{(.+?)\}\}/g
            // }
          // },
          files: {
            // "path/to/compiled/templates.js": ["path/to/source/**/*.html"]
            "public/js/app/templates.js": ["public/js/app/templates/**/*.html"]
          }
        }
      },
      // configure jshint to validate js files -----------------------------------
      jshint: {
        options: {
          reporter: require('jshint-stylish') // use jshint-stylish to make our errors look and read good
        },

        // when this task is run, lint the Gruntfile and all js files in src
        build: ['Gruntfile.js', 'public/js/**/*.js']
      },
      less: {
        development: {
          options: {
            compress: true,
            yuicompress: true,
            optimization: 2
          },
          files: {
            "public/css/main.css": "public/less/main.less" // destination file and source file
          }
        }
      },
      watch: {
        styles: {
          files: ['public/less/**/*.less', 'public/js/**/*.js', "public/js/app/templates/**/*.htm"], // which files to watch
          tasks: ['less', 'jshint', 'jst'],
          options: {
            nospawn: true
          }
        }
      }

    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    // we can only load these if they are in our package.json
    // make sure you have run npm install so our app can find these
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jst');

    // ===========================================================================
    // REGISTER TASKS ============================================================
    // ===========================================================================
    grunt.registerTask('default', ['jst', 'less', 'jshint', 'watch']);
}