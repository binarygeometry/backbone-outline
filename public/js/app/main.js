/*! waitForImages jQuery Plugin - v1.4.1 - 2012-10-12
* https://github.com/alexanderdickson/waitForImages
* Copyright (c) 2012 Alex Dickson; Licensed MIT */
(function(e){var t="waitForImages";e.waitForImages={hasImageProperties:["backgroundImage","listStyleImage","borderImage","borderCornerImage"]},e.expr[":"].uncached=function(t){if(!e(t).is('img[src!=""]'))return!1;var n=new Image;return n.src=t.src,!n.complete},e.fn.waitForImages=function(n,r,i){var s=0,o=0;e.isPlainObject(arguments[0])&&(n=arguments[0].finished,r=arguments[0].each,i=arguments[0].waitForAll),n=n||e.noop,r=r||e.noop,i=!!i;if(!e.isFunction(n)||!e.isFunction(r))throw new TypeError("An invalid callback was supplied.");return this.each(function(){var u=e(this),a=[],f=e.waitForImages.hasImageProperties||[],l=/url\(\s*(['"]?)(.*?)\1\s*\)/g;i?u.find("*").andSelf().each(function(){var t=e(this);t.is("img:uncached")&&a.push({src:t.attr("src"),element:t[0]}),e.each(f,function(e,n){var r=t.css(n),i;if(!r)return!0;while(i=l.exec(r))a.push({src:i[2],element:t[0]})})}):u.find("img:uncached").each(function(){a.push({src:this.src,element:this})}),s=a.length,o=0,s===0&&n.call(u[0]),e.each(a,function(i,a){var f=new Image;e(f).bind("load."+t+" error."+t,function(e){o++,r.call(a.element,o,s,e.type=="load");if(o==s)return n.call(u[0]),!1}),f.src=a.src})})}})(jQuery);

// set a boolean variable
var booly = false;
// event propigation helper
var pubSub = _.extend({},Backbone.Events);

// current time in correct format helper
function now() {
  return (new Date()).getTime();
}


// Set our application namespace
var workSpace = workSpace || {};

/* router
// This is the routing system
// currently contains one route
*/
workSpace.Router = Backbone.Router.extend({
    routes: {
        '': 'homeCtrl'
    },
 
    homeCtrl: function(){
      console.log('begin')
      // reference to the containing class
      var that = this
      // set a new collection of tasks
      var task = new workSpace.TaskCollection();

      console.log(task)

      // loads a collection of tasks - useless now but will be helpful when we add save feature
      // task.fetch().then( function(resp) {// fetch client collection from db
        console.log(task);
        that.view = new workSpace.taskControlView();
        that.view = new workSpace.taskDisplayView({ collection: task });
      // });
    }
});

/* Task
// This is the model for a Task. It currently mocks a uid for the user since there is no data persistance layer
*/

workSpace.Task = Backbone.Model.extend({
  defaults: {
    user_id: 'mock_uid',
    start_time: 0,
    stop_time: 0,
    total_time: 0,
    task: null
  },
  initialize: function(){
    console.log("New task created");

    // useful subscription pattern
    // this.on("change:stop", function(model){
    //     // model.displayStatus()
    //     console.log('is this an anti pattern?');
    // });
  },
  url : function() {
    // Important! It's got to know where to send its REST calls. 
    // In this case, POST to '/desk' and PUT to '/desk/:id'
    return this.id ? '/api/desk/' + this.id : '/api/desk'; 
  },
  // Set the current time and update model with task
  begin: function(){
    var start_time = now();
    console.log('starttime', start_time)
    this.set({"start_time": start_time});
    this.save()
  },
  stop: function () {
    var stop_time = now();
    var start_time = this.get('start_time');
    var total_time = stop_time - start_time;
    this.set({ stop_time: stop_time, total_time: total_time });
    this.save()
  }
});

/* TaskCollection
// This is collection which will house a list of task
*/
workSpace.TaskCollection = Backbone.Collection.extend({
  model : workSpace.Task,
  url : "/api/desk"
});
    
/* taskControlView
// Contains the buttons. Does publish stuff
*/
workSpace.taskControlView = Backbone.View.extend({
  template: _.template($("#task-control-view").html()),

  el: '#task-control',

  initialize: function() {
    this.render();
  },

  logic: false,

  task: {},

  render: function() {
    this.task = new workSpace.Task();
    // mow down zommbies    
    $(this.el).empty();
    
    var html = this.template();
    $(this.el).html(html);
  },

  events: {
    "click #go": "startTimer",
    "click #done": "stopTimer"
    // "change select": "activateFilter"
  },
  
  startTimer: function(){
    var that = this
    // set logic
    that.logic = true
    // create new task 
    var currentTask = $('#new-task').val();
    console.log('currentTask',currentTask)
    // save current time to model
    that.task.set({task, currentTask})

    that.task.begin();

    // show timer animation
    $(".timer").toggleClass("timing");
    // hide task form
    $('.taskList').slideUp();
    // reset task
    $('#new-task').val("");
    // save task 
    that.task.save().then(function(stuff){
      console.log('good', stuff);
      var uid = stuff.uid
    });
  },

  stopTimer: function() {
    if(this.logic){
      console.log('filter', this.task)
      
      this.task.stop();
      // stop timer animation
      $(".timer").toggleClass("timing");
      // show task form
      $('.taskList').slideDown();
      this.task.save();
    }
    // var filter = $('select').val();
    // pubSub.trigger("foo:thing",{
    //   filter : filter
    // });
        // do something on select change
  }

});

/* taskDisplayView 
// A repeatable task view. Does subscribe stuff
*/
workSpace.taskDisplayView = Backbone.View.extend({
  template: _.template($("#task-view-display-task").html()),
  // template: _.template(window.JST["public/js/app/templates/desk-view-main.html"](), {"owner": owner, "sector": sector}),
  // _.template(window.JST['menu'](),{ user: a, loggedIn: l })
  el: '#task-collection-view',

  initialize: function() {
    this.render();
    // this.listenTo(pubSub,"foo:thing",this.bla,this);

  },
  // bla: function(jfkld){
    // console.log('call', jfkld);
    // var filter = jfkld.filter;
    // this.render(filter)
  // },

  render: function(a) {

    var that = this
    // // mow down zommbies    
    $(this.el).empty();
    
    // return the collection data with a promise
    that.collection.fetch().then(function(){
      // iterate over the collection object
      that.collection.each(function(task){
        console.log('task',task)
        var dict = {
          user: task.get('user_id'),
          start_time: task.get('start_time'),
          task: task.get('task')
        }
        console.log(dict)
        var html = that.template(dict);
        $(that.el).append(html);
      });
    });
    console.log('end')
  }
      // $(".taskList ul li").fadeOut(700, function(){
      //   $(this).prependTo(".doneList ul").css('marginTop',-69).show().animate({marginTop:0},4000);
      //   });

});


(function ($) {
  console.log('one')
    router = new workSpace.Router();
    Backbone.history.start();
})(jQuery);
// delay loading animation until everything is loaded
$('body').waitForImages(function(){

    $('.page').delay(1100).show();

    $('.instructions').delay(2400).animate({marginTop:0},'slow');

    waitForAll: true
  
    console.log('two')


    // Start our application when everything is ready
});





// the fun stuff
  //     $(".edit").removeClass("edit").unbind("click.editable").addClass("edited");
  //     $(".timer").toggleClass("timing");
  //     function update_output() {
    
  //       var out  = [],
  //         displayTime;
        
  //       for (var i = 0, len = clicks.length; i < len; i++) {
  //         displayTime = (clicks[i].time - startTime);
  //         console.log(displayTime);
  //         seconds = displayTime
        
  //         outputTime = "I spent " + Math.floor(displayTime / 60000) + " Minutes on this task";
  //       }
        
  //       $(".taskList ul li .time").html(outputTime);
  //     }
    
  //     var clicks = [];
    
  //     clicks.push({ time : new Date().getTime(), target : $(this).attr('href') });
  //     update_output();
    
    

    
      // $(".taskList ul")
        // .html("<li class='inner'><div class='task'><div class='edit'>..That wasn't in the spec.</div></div><div class='time'></div></li>");
    

  // }








// var desk = new workSpace.Desk({ sector: 72, desk: 61, owner: 'Space SubLets'});
// desk.save();
// var desk = new workSpace.Desk({ sector: 42, desk: 31, owner: 'The Competition'});
// desk.save();
// desk.displayStatus();
// desk.book('674563'); 
// desk.displayStatus();
// desk.free(); 
// desk.displayStatus();













  // $("#done").click(function(e){
  //   if (booly === true){

  //     // $(".edit").editable("echo.php", {
  //     //   placeholder : "..That wasn't in the spec.",
  //     //   loadtype : "POST",
  //     //   callback: function(value, settings) {
  //     //     startTime = now();
  //     //     console.log(startTime);
  //     //     $(".timer").toggleClass("timing");
  //     //     booly = true;
  //     //     return false;//
  //     //   }  
  //     // });
      
  //     booly = false;
  //     return false;
      
  //   } else {
    
  //     alert("you have not entered a task");
  //     return false;
  //   }
  // });
  
  // $('.results').click(function(){
    
  //   alert('works');
  //   return false;
  // });
  
  
 // start timer
//   create new task
//   start animation
//   hide go button
// stop task
//   calculate time
//   append new task to collection
//   reset view