this["JST"] = this["JST"] || {};

this["JST"]["public/js/app/templates/desk-view-main.html"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '  <nav id="tweak"></nav>\n  <div class="desk">\n    <h3>Workspace:</h3>\n    <strong>Sector: ' +
((__t = ( sector )) == null ? '' : __t) +
', owned by ' +
((__t = ( owner )) == null ? '' : __t) +
'</strong>\n    <h3>end Workspace:</h3>\n  </div>\n</div>\n';

}
return __p
};