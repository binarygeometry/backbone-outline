
  // Place JavaScript code here...
  
// Declare app level module which depends on filters, and services
var KgTech = angular.module('KgTech', ['underscore'])

// var KgTechUrl = 'http://answersonapostcard-47329.onmodulus.net'
var KgTechUrl = 'hsieh.i:3000'
  // 'angular-underscore'

// The controller
var FilterCtrl = KgTech.controller('FilterCtrl', ['$scope', 'theService', 'mySharedService','_',
    function($scope, theService, mySharedService,_, ngCart) {

      $scope.debug = 'FilterCtrl'

      $scope.display = 'all'

      // $scope.items = theService.brands
      theService.shops().then(function(data){
        $scope.shops = data 

      })
      console.log($scope.shops)


      // $scope.arrangedItems = usefulitem($scope.items, 'all');
      // $('.filter-all').addClass('active');

      $scope.$on('handleBroadcast', function() {
          var index =  mySharedService.message;
          $scope.display = mySharedService.message;
          $scope.arrangedItems = usefulitem($scope.items, index);
      });


}])
      // https://tenthousandmistakes.wordpress.com/
     function usefulitem (items, filter){
        var usefulObject = []
        // marker array
        var categories = []

        for(var i = 0; i < items.length; i++){
          var currentItem = items[i]
          console.log(currentItem)

          var cat   = currentItem.value
          var brand = currentItem.brand
          var tags  = currentItem.tags

          // var cats = [{cat:'cat name', brands:[{name:'kg', tags:['all']}]}]

          // construct our container
          if(!_.contains(categories, cat) ){  
            categories.push(cat)
            usefulObject.push({"cat":cat,"brands":[]})
            
          }
          if(_.contains(tags, filter) ){
            var thing = _.findWhere(usefulObject, {cat: cat})
            thing.brands.push({"name":brand,"tags":tags})
            console.log(usefulObject)
          }

          
          
        }
        return usefulObject
      }


KgTech.factory('mySharedService', function($rootScope) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    return sharedService;
});


KgTech.factory('theService', function($http, $q) {

  var factory = {}

  factory.filters = [{"name":'all'}, {"name":'women'}, {"name":'men'}, {"name":'bags'}, {"name":'misc'}]

  factory.shops = function(){

    var deferred = $q.defer()
    
    $http.get(KgTechUrl +'/api/shops/').then(function(resp) {
      console.log(resp.data)
      deferred.resolve(resp.data)
    })

    return deferred.promise
  }


  // [
  //   {
  //     "owner":"me",
  //     "products" : [
  //       {
  //       "name"    : "Goodwill PunTing", 
  //       "price"   : "20", 
  //       "desc"    : "this book is great",
  //       "image"   : "http://41.media.tumblr.com/4a89bf5a7df607deb257c492e8669ba2/tumblr_noagm6ZKGM1rqqtpjo1_500.jpg",
  //       "bitcoin" : "usdf77dsf8ds6f8678sf6d7f",
  //       "paypal"  : "asianchews@totallyrooked.com", 
  //       },
  //       {
  //       "name"    : "Ladle of Filth", 
  //       "price"   : "20", 
  //       "desc"    : "An unsexy guide to pop magick",
  //       "image"   : "imageurl.png",
  //       "bitcoin" : "usdf77dsf8ds6f8678sf6d7f",
  //       "paypal"  : "asianchews@totallyrooked.com", 
  //       },
  //       {
  //       "name"    : "Bad Grimoir", 
  //       "price"   : "10", 
  //       "desc"    : "The prequel to Enough Money - how we finally made it",
  //       "image"   : "imageurltwo.png",
  //       "bitcoin" : "usdf77dsf8ds6f8678sf6d7f",
  //       "paypal"  : "asianchews@totallyrooked.com", 
  //       }
  //     ] 
  //   },
  //   {
  //     "owner":"otherpeople",
  //     "products" : [
  //       {
  //       "name"    : "ABC book of Drugs", 
  //       "price"   : "10", 
  //       "desc"    : "Agnositic toddler proof guide to drugs",
  //       "image"   : "http://41.media.tumblr.com/4a89bf5a7df607deb257c492e8669ba2/tumblr_noagm6ZKGM1rqqtpjo1_500.jpg",
  //       "bitcoin" : "usdf77dsf8ds6f8678sf6d7f",
  //       "paypal"  : "asianchews@totallyrooked.com", 
  //       }
  //     ] 
  //   }

  // ]

  return factory

})

// /* this bit */
KgTech.directive('filterlist', function(theService, mySharedService) {
    return {
        restrict: 'E',
        // scope: {filters: '@myAttr'},
        controller: function($rootScope, $scope, $attrs, theService, mySharedService) {
          console.log(theService)
          console.log(mySharedService)
          $scope.filters = theService.filters;
          console.log('interestlist',$scope.filters)

          $scope.change = function(filtername){
            console.log(filtername);
            $('.tab').removeClass('active');
            $('.filter-'+filtername).addClass('active');
            mySharedService.prepForBroadcast(filtername)
          }
        },
        replace: true,
        template: '<ul class="tabs list-inline"><li ng-repeat="filter in filters" class="tab filter-{{filter.name}}"><a href="#" alt="{{filter.name}}" ng-click="change(filter.name)">{{filter.name}}</a></li></ul>'
    };
});

KgTech.directive('filter', function(theService, mySharedService) {
    return {
        restrict: 'E',
        // scope: {filters: '@myAttr'},
        controller: function($rootScope, $scope, $attrs, theService, mySharedService) {
          console.log(theService)
          console.log(mySharedService)
          $scope.filters = theService.filters;
          console.log('interestlist',$scope.filters)

          $scope.change = function(filtername){
            console.log(filtername);
            $('.tab').removeClass('active');
            $('.filter-'+filtername).addClass('active');
            mySharedService.prepForBroadcast(filtername)
          }
        },
        replace: true,
        template: '<ul class="tabs list-inline"><li ng-repeat="filter in filters" class="tab filter-{{filter.name}}"><a href="#" alt="{{filter.name}}" ng-click="change(filter.name)">{{filter.name}}</a></li></ul>'
    };
});

var shopCtrl = KgTech.controller('shopCtrl', ['$scope', 'shopFactory',
    function($scope, shopFactory) {

      $scope.init = function(owner){

        console.log(owner)
        shopFactory.getShop(owner).then(function(data){
          $scope.shop = data
          console.log(data)
        })
        simpleCart.empty();
        simpleCart({
          checkout: {
            type: "PayPal",
            email: "chinesechews@gmail.com"
          }
        });

          // var win = $window;
          // $scope.$watch('myForm.$dirty', function(value) {
          //   if(value) {
          //     win.onbeforeunload = function(){
          //       return 'Your message here';
          //     };
          //   }
          // });


      }

        
      // console.log($scope.items)


      // $scope.arrangedItems = usefulitem($scope.items, 'all');
      // $('.filter-all').addClass('active');

      // $scope.$on('handleBroadcast', function() {
          // var index =  mySharedService.message;
          // $scope.display = mySharedService.message;
          // $scope.arrangedItems = usefulitem($scope.items, index);
      // });


}])

/* this bit */
// AngularBonfire.directive('interestlist', function(theService) {
//     return {
//         restrict: 'E',
//         scope: {username: '@myAttr'},
//         controller: function($scope, $attrs, $q, theService, mySharedService) {
//                     // {localName: '@myAttr'},
//                       // $scope.stuff = theService.getAllByUsername($scope.localN)
//           var username = $scope.username
//           var defer = $q.defer() 
//           defer.resolve(theService.getAllByUsername(username));
//           defer.promise.then(function (data) {
//               $scope.data = data;
//               console.log($scope.data);
//           $scope.stuff = data //theService.getAllByUsername('testtest')
//             });
//           // $scope.stuff = theService.getAllByUsername($scope.localN)
//           // console.log($scope.stuff);
//           $scope.doStuff = function(input) {
//             console.log(input)
//             mySharedService.prepForBroadcast(input)
//           }
//             // $scope.$on('handleBroadcast', function() {
//               // var index =  mySharedService.message;
//                  // $scope.display = $scope.list[index] //'Directive: ' + mySharedService.message;
//             // });
//         },
//         replace: true,
//         // template: '<h1>sdfsdf{{th}}</h1>'
//         template: '<ul class="vertical-nav list-unstyled"><li ng-repeat="list in stuff track by $index"><a href="#" ng-click="doStuff($index)">{{list.name}}</a></li></ul>'
//         // template: '<p><h2>{{display.name}}</h2><p>g{{display.list}}</p></article>'
//     };
// });

// var KgTechShopTemplate = 
// '<section class="shop">'
// +'<div class="row"><div class="col-sm-12">data.owner.name</div></div>'
// +'<div class="row sales-grid"><article class="col-sm-4" ng-repeat="product in data.products">'
// +'<product data="{{product}}"></product>'
// +'</div></div>'
// +'</section>'
// /* this bit */
// KgTech.directive('shop', function(shopFactory) {
//     return {
//         restrict: 'E',
//         scope: {owner: '@owner'},
//         controller: function($scope, $attrs, $q, shopFactory) {

//           // save the onwer _id passed from our jade view
//           var owner = $scope.owner
//           // set up a promise object
//           var defer = $q.defer() 
//           // resolve the promise with a call to our service
//           defer.resolve(shopFactory.getShop(owner));
//           // create scope data for our shop template
//           defer.promise.then(function (data) {
//               $scope.data = data;
//               console.log($scope.data);
//               $scope.list = data 
//           });
//           // // console.log($scope.list);

//           //   $scope.$on('handleBroadcast', function() {
//           //     var index =  mySharedService.message;
//           //        $scope.display = $scope.list[index] //'Directive: ' + mySharedService.message;
//           //   });

//         },
//         replace: true,
//         template: KgTechShopTemplate 
                    
//     };
// });


// var KgTechProductTemplate =
// '<article class="simpleCart_shelfItem">{{product}}{{product.paypal}}'
//   +'<span class="item_price">£{{product.price}}</span>'
//   +'<img src="{{product.image}}" alt="{{product.name}}" />'
//   +'<div class="inner-contentedness">'
//     +'<h4 class="item_name">{{product.name}}</h4>'
//     +'<input class="item_add btn" type="button" value="Add to cart">/'
//     +'<input class="item_quantity" type="hidden" value="1"/>'
//     +'<p>{{product.desc}}</p>'
//   +'</div>'
// +'</article>'
// KgTech.directive('product', function($compile) {
//     return {
//         restrict: 'E',
//         scope: {product: '@data'},
//         controller: function($scope, $attrs) {
//           console.log($scope.product)
//           // $scope.apply()
//         },
//       //   compile: function compile(tElement, tAttrs, transclude) {
//       // return {
//       //   // pre: function preLink(scope, iElement, iAttrs, controller) { ... },
//       //   post: function postLink(scope, iElement, iAttrs, controller) { 
//       //     console.log(scope.product) // ... 
//       //     var t = scope.product
//       //     console.log(typeof t)
//       //     console.log(JSON.parse(t))
//       //     var x = angular.element(KgTechProductTemplate);
//       //           iElement.append(x);
//       //           $compile(x)(scope);

//       //   }
//       // }
//       // or
//       // return function postLink( ... ) { ... }
//     // },
//         replace: true,
//         template: KgTechProductTemplate
//         // template: '<article><h4>{{display.name}}</h4><p>{{display.description}}</p><p>{{display.rating}}</p></article>'
//         // template: '<p><h2>{{display.name}}</h2><p>{{display.list}}</p></article>'
//     };
// });

KgTech.factory('shopFactory', function($http, $q) {

  var factory = {}

  factory.filters = [{"name":'all'}, {"name":'women'}, {"name":'men'}, {"name":'bags'}, {"name":'misc'}]

  factory.getShop = function(owner){

    var deferred = $q.defer()
    
    $http.get(KgTechUrl +'/api/shop/'+ owner).then(function(resp) {
      
      deferred.resolve(resp.data)
    })

    return deferred.promise
  }

  // factory.payWithCoins = function(wallet, id){

  //   var receiving_address = '828cd12b-d3b4-4c93-b501-c769bd379bde'  
  //   var callback_url = KgTechUrl+'/payments/'//+id
  //   var blockchainUrl = 'https://blockchain.info/api/receive?method=create&address='+receiving_address+'&callback='+callback_url

  //   var deferred = $q.defer()

  //   $http.get(blockchainUrl).then(function(resp) {
      
  //     deferred.resolve(resp.data)
  //   })

  //   return deferred.promise
  // }


  return factory

})


