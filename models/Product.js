
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var BearSchema   = new Schema({
    owner   : String,
    name    : String,
    price   : Number,
    desc    : String,
    image   : String,
    paypal  : String,
    bitcoin : String,
});

module.exports = mongoose.model('Bear', BearSchema);

    // paypal: Boolean,
    // paypalId: String,
    // btc: Boolean,
    // btcId: String