var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var DeskSchema = new Schema({
    user_id: String,
    start_time: Number,
    stop_time: Number,
    total_time: Number,
    task: String,
});

module.exports = mongoose.model('Desk', DeskSchema);
